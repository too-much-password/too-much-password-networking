import cst from './constant';

const initialState = {running: 0};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case cst.START:
      return {
        running: state.running + 1
      };
    case cst.STOP:
      return {
        running: state.running - 1
      };
    default:
      return state;
  }
}
