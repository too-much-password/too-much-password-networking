import merge from 'lodash.merge';
import fetch from 'fetch-everywhere';
import Promise from 'bluebird';

import cst from './constant';

export const start = () => ({type: cst.START});
export const stop = () => ({type: cst.STOP});

export default function request(url, options) {
  return (dispatch, getState) => {
    dispatch(start());
    let requestOptions = merge({
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }, options);
    for (let key in requestOptions.headers) {
      if (requestOptions.headers[key] == null) {
        delete requestOptions.headers[key];
      }
    }
    let {authentication} = getState();
    if (authentication && authentication.id) {
      requestOptions.headers['Authorization'] = authentication.id;
    }
    let status;
    return Promise
      .resolve(fetch(url, requestOptions))
      .then((response) => {
        status = response.status;
        return status === 204 ? null : response.json();
      })
      .then((response) => {
        let result = {status, data: response};
        if (status >= 200 && status < 300) {
          return result;
        } else {
          let err = new Error(response.error ? response.error.name : JSON.stringify(response));
          err.data = response;
          throw err;
        }
      })
      .finally(() => dispatch(stop()));
  };
};
