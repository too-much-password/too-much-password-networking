'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.stop = exports.start = undefined;
exports.default = request;

var _lodash = require('lodash.merge');

var _lodash2 = _interopRequireDefault(_lodash);

var _fetchEverywhere = require('fetch-everywhere');

var _fetchEverywhere2 = _interopRequireDefault(_fetchEverywhere);

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _constant = require('./constant');

var _constant2 = _interopRequireDefault(_constant);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var start = exports.start = function start() {
  return { type: _constant2.default.START };
};
var stop = exports.stop = function stop() {
  return { type: _constant2.default.STOP };
};

function request(url, options) {
  return function (dispatch, getState) {
    dispatch(start());
    var requestOptions = (0, _lodash2.default)({
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }, options);
    for (var key in requestOptions.headers) {
      if (requestOptions.headers[key] == null) {
        delete requestOptions.headers[key];
      }
    }

    var _getState = getState();

    var authentication = _getState.authentication;

    if (authentication && authentication.id) {
      requestOptions.headers['Authorization'] = authentication.id;
    }
    var status = void 0;
    return _bluebird2.default.resolve((0, _fetchEverywhere2.default)(url, requestOptions)).then(function (response) {
      status = response.status;
      return status === 204 ? null : response.json();
    }).then(function (response) {
      var result = { status: status, data: response };
      if (status >= 200 && status < 300) {
        return result;
      } else {
        var err = new Error(response.error ? response.error.name : JSON.stringify(response));
        err.data = response;
        throw err;
      }
    }).finally(function () {
      return dispatch(stop());
    });
  };
};