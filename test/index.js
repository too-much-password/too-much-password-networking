const {expect} = require('chai');
import {action, reducer, constant} from '../source/index.js';

describe('index', function() {

  it('should expose action', () => {
    expect(action).to.be.ok;
  });

  it('should expose reducer', () => {
    expect(reducer).to.be.ok;
  });

  it('should expose constant', () => {
    expect(constant).to.be.ok;
  });

});
